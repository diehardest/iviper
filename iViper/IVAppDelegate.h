//
// Created by VIPER
// Copyright (c) 2015 VIPER. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IVAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

