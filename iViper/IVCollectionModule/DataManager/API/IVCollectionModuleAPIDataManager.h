//
// Created by VIPER.
// Copyright (c) 2015 VIPER. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IVCollectionModuleProtocols.h"


@interface IVCollectionModuleAPIDataManager : NSObject <IVCollectionModuleAPIDataManagerInputProtocol>

- (void)fetchMediaWithCompletion:(void (^)(NSArray *media, NSError* error))completion;

@end