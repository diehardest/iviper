//
// Created by VIPER
// Copyright (c) 2015 VIPER. All rights reserved.
//

#import "InstagramEngine.h"
#import "IVCollectionModuleAPIDataManager.h"

@implementation IVCollectionModuleAPIDataManager

#pragma mark - IVCollectionModuleAPIDataManagerInputProtocol

- (void)fetchMediaWithCompletion:(void (^)(NSArray *media, NSError* error))completion;
{
    [[InstagramEngine sharedEngine] getPopularMediaWithCount:31
                                                     success:^(NSArray *media, InstagramPaginationInfo *paginationInfo)
    {
         if (completion)
         {
             completion(media, nil);
         }
     }
     failure:^(NSError *error)
     {
         if (completion)
         {
             completion(nil, error);
         }
     }];
}

@end