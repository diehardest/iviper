//
// Created by VIPER
// Copyright (c) 2015 VIPER. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IVCollectionModuleProtocols.h"


@interface IVCollectionModuleInteractor : NSObject <IVCollectionModuleInteractorInputProtocol>

@property (nonatomic, weak) id <IVCollectionModuleInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <IVCollectionModuleAPIDataManagerInputProtocol> APIDataManager;

- (void)requestImageURLs;

@end