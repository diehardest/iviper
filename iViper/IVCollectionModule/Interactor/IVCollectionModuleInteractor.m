//
// Created by VIPER
// Copyright (c) 2015 VIPER. All rights reserved.
//

#import <Reachability.h>
#import "IVCollectionModuleInteractor.h"

@implementation IVCollectionModuleInteractor

#pragma mark - IVCollectionModuleInteractorInputProtocol
- (void)requestImageURLs
{
    if ([Reachability reachabilityForInternetConnection].isReachable)
    {
        [self.APIDataManager fetchMediaWithCompletion:^(NSArray *media, NSError *error)
         {
             if (error)
             {
                 [self.presenter reciveError:error];
             }
             else
             {
                 [self.presenter updateImageURLs:[media valueForKeyPath:@"thumbnailURL"]];
             }
         }];
    }
    else
    {
        [self.presenter loseConnection];
    }
}

@end