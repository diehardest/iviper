//
// Created by VIPER
// Copyright (c) 2015 VIPER. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IVCollectionModuleProtocols.h"

@class IVCollectionModuleWireFrame;

@interface IVCollectionModulePresenter : NSObject <IVCollectionModulePresenterProtocol, IVCollectionModuleInteractorOutputProtocol>

@property (nonatomic, weak) id <IVCollectionModuleViewProtocol> view;
@property (nonatomic, strong) id <IVCollectionModuleInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <IVCollectionModuleWireFrameProtocol> wireFrame;

- (void)updateImageURLs:(NSArray*)imageURLs;
- (void)reciveError:(NSError*)error;
- (void)loseConnection;

@end
