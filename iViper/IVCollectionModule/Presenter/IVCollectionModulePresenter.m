//
// Created by VIPER
// Copyright (c) 2015 VIPER. All rights reserved.
//

#import "IVCollectionModulePresenter.h"
#import "IVCollectionModuleWireframe.h"

@implementation IVCollectionModulePresenter

#pragma mark - IVCollectionModulePresenterProtocol
- (void)updateView
{
    [self.view lockUI];
    [self.interactor requestImageURLs];
}

#pragma mark - IVCollectionModuleInteractorOutputProtocol
- (void)updateImageURLs:(NSArray*)imageURLs
{
    [self.view unlockUI];
    [self.view showImages:imageURLs];
}

- (void)reciveError:(NSError*)error
{
    [self.view unlockUI];
    [self.view showErrorMessage:error.localizedDescription];
}

- (void)loseConnection
{
    [self.view unlockUI];
    [self.view showNoConnectionAlert];
}

@end