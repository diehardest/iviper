//
// Created by VIPER
// Copyright (c) 2015 VIPER. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol IVCollectionModuleInteractorOutputProtocol;
@protocol IVCollectionModuleInteractorInputProtocol;
@protocol IVCollectionModuleViewProtocol;
@protocol IVCollectionModulePresenterProtocol;
@protocol IVCollectionModuleAPIDataManagerInputProtocol;


@class IVCollectionModuleWireFrame;

// PRESENTER -> VIEWCONTROLLER
@protocol IVCollectionModuleViewProtocol

@required

@property (nonatomic, strong) id <IVCollectionModulePresenterProtocol> presenter;

- (void)showImages:(NSArray*)urlDataSource;
- (void)lockUI;
- (void)unlockUI;
- (void)showNoConnectionAlert;
- (void)showErrorMessage:(NSString*)message;

@end

// PRESENTER -> WIREFRAME
@protocol IVCollectionModuleWireFrameProtocol

@required

+ (void)presentIVCollectionModuleModuleFromWindow:(UIWindow *)window;

@end

// VIEWCONTROLLER -> PRESENTER
@protocol IVCollectionModulePresenterProtocol

@required

@property (nonatomic, weak) id <IVCollectionModuleViewProtocol> view;
@property (nonatomic, strong) id <IVCollectionModuleInteractorInputProtocol> interactor;
@property (nonatomic, strong) id <IVCollectionModuleWireFrameProtocol> wireFrame;

- (void)updateView;

@end

// INTERACTOR -> PRESENTER
@protocol IVCollectionModuleInteractorOutputProtocol

- (void)updateImageURLs:(NSArray*)imageURLs;
- (void)reciveError:(NSError*)error;
- (void)loseConnection;

@end

// PRESENTER -> INTERACTOR
@protocol IVCollectionModuleInteractorInputProtocol

@required

@property (nonatomic, weak) id <IVCollectionModuleInteractorOutputProtocol> presenter;
@property (nonatomic, strong) id <IVCollectionModuleAPIDataManagerInputProtocol> APIDataManager;

- (void)requestImageURLs;

@end

// INTERACTOR -> DATAMANAGER
@protocol IVCollectionModuleDataManagerInputProtocol

@required

- (void)fetchMediaWithCompletion:(void (^)(NSArray *media, NSError* error))completion;

@end

// INTERACTOR -> APIDATAMANAGER
@protocol IVCollectionModuleAPIDataManagerInputProtocol <IVCollectionModuleDataManagerInputProtocol>

@end

