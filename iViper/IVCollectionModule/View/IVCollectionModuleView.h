//
// Created by VIPER
// Copyright (c) 2015 VIPER. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IVCollectionModuleProtocols.h"

@interface IVCollectionModuleView : UICollectionViewController <IVCollectionModuleViewProtocol>

@property (nonatomic, strong) id <IVCollectionModulePresenterProtocol> presenter;

+ (instancetype)collectionModuleView;
- (void)showImages:(NSArray*)urlDataSource;
- (void)lockUI;
- (void)unlockUI;
- (void)showNoConnectionAlert;
- (void)showErrorMessage:(NSString*)message;

@end
