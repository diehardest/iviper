//
// Created by VIPER
// Copyright (c) 2015 VIPER. All rights reserved.
//

#import <FMMosaicLayout.h>
#import <MBProgressHUD.h>
#import <UIAlertView+Blocks.h>
#import "IVCollectionModuleView.h"
#import "IVCollectionModuleViewCell.h"

@interface IVCollectionModuleView () <FMMosaicLayoutDelegate>

@property (nonatomic, strong) NSArray* urlDataSource;

@property (nonatomic, strong) UIBarButtonItem* reloadButton;

@end

@implementation IVCollectionModuleView

#pragma mark - ViewController Lifecycle

+ (instancetype)collectionModuleView
{
    return [[IVCollectionModuleView alloc] initWithCollectionViewLayout:[FMMosaicLayout new]];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configureView];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.presenter updateView];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)configureView
{
    self.navigationItem.title = @"iViper";
    self.collectionView.layer.needsDisplayOnBoundsChange = YES;
    [self.collectionView registerClass:IVCollectionModuleViewCell.class
            forCellWithReuseIdentifier:IVCollectionModuleViewCell.ID];
    self.collectionView.backgroundColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"texture"]];
    self.reloadButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh
                                                                      target:self
                                                                      action:@selector(reloadClick)];
    self.navigationItem.rightBarButtonItem = self.reloadButton;
    UITapGestureRecognizer *doubleTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                       action:@selector(doubleTap:)];
    [doubleTapGesture setNumberOfTapsRequired:2];
    [doubleTapGesture setNumberOfTouchesRequired:1];
    [self.view addGestureRecognizer:doubleTapGesture];
}

#pragma mark - Actions

- (void)reloadClick
{
    [self.presenter updateView];
}

- (void)doubleTap:(UITapGestureRecognizer *)sender
{
    if (sender.state == UIGestureRecognizerStateEnded)
    {
        CGPoint point = [sender locationInView:self.collectionView];
        NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:point];
        if (indexPath)
        {
            [self.collectionView scrollToItemAtIndexPath:indexPath
                                        atScrollPosition:UICollectionViewScrollPositionCenteredVertically
                                                animated:YES];
            IVCollectionModuleViewCell* cell = (IVCollectionModuleViewCell*)[self.collectionView cellForItemAtIndexPath:indexPath];
            cell.layer.zPosition = 1;
            [cell animateWithCompletion:^
            {
                cell.layer.zPosition = 0;
            }];
        }
    }
}

#pragma mark - IVCollectionModuleViewProtocol

- (void)showImages:(NSArray*)urlDataSource
{
    _urlDataSource = urlDataSource;
    [self.collectionView reloadData];
}

- (void)lockUI
{
    self.reloadButton.enabled = NO;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

- (void)unlockUI
{
    self.reloadButton.enabled = YES;
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void)showNoConnectionAlert
{
    [self showErrorMessage:@"Please, check your Internet connection"];
}

- (void)showErrorMessage:(NSString*)message
{
    RIButtonItem* retryButton = [RIButtonItem itemWithLabel:@"Retry" action:^
    {
        [self.presenter updateView];
    }];
    [[[UIAlertView alloc] initWithTitle:@"Oops!"
                                message:message
                       cancelButtonItem:retryButton
                       otherButtonItems:nil] show];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.urlDataSource.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    IVCollectionModuleViewCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:IVCollectionModuleViewCell.ID
                                                                                 forIndexPath:indexPath];
    cell.layer.needsDisplayOnBoundsChange = YES;
    cell.imageURL = self.urlDataSource[indexPath.row];
    return cell;
}

#pragma mark - FMMosaicLayoutDelegate

- (NSInteger)collectionView:(UICollectionView *)collectionView layout:(FMMosaicLayout *)collectionViewLayout numberOfColumnsInSection:(NSInteger)section
{
    return 2;
}

- (FMMosaicCellSize)collectionView:(UICollectionView *)collectionView layout:(FMMosaicLayout *)collectionViewLayout
  mosaicCellSizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return (indexPath.item % 12 == 0) ? FMMosaicCellSizeBig : FMMosaicCellSizeSmall;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(FMMosaicLayout *)collectionViewLayout
        insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsZero;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(FMMosaicLayout *)collectionViewLayout
interitemSpacingForSectionAtIndex:(NSInteger)section
{
    return 2.0;
}

@end