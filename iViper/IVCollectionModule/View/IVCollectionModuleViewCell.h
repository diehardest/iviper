//
//  IVCollectionModuleViewCell.h
//  iviper
//
//  Created by Dmitriy Gorbel on 4/13/15.
//  Copyright (c) 2015 Dmitriy Gorbel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IVCollectionModuleViewCell : UICollectionViewCell

@property (strong, nonatomic) NSURL* imageURL;

+ (NSString*)ID;
- (void)animateWithCompletion:(void(^)(void))completion;

@end
