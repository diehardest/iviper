//
//  IVCollectionModuleViewCell.m
//  iviper
//
//  Created by Dmitriy Gorbel on 4/13/15.
//  Copyright (c) 2015 Dmitriy Gorbel. All rights reserved.
//

#import <Haneke.h>
#import <POPAnimation.h>
#import <POPSpringAnimation.h>
#import "IVCollectionModuleViewCell.h"

@interface IVCollectionModuleViewCell ()

@property (strong, nonatomic) UIImageView* imageView;

@end

@implementation IVCollectionModuleViewCell

+ (NSString *)ID
{
    return @"IVCollectionModuleViewCellID";
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.imageView = [[UIImageView alloc] initWithFrame:self.contentView.bounds];
        [self.contentView addSubview:self.imageView];
        [self configureConstraints];
    }
    return self;
}

- (void)configureConstraints
{
    [self.imageView setTranslatesAutoresizingMaskIntoConstraints:NO];

    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.imageView
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.contentView
                                                          attribute:NSLayoutAttributeTop
                                                         multiplier:1
                                                           constant:0]];

    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.imageView
                                                          attribute:NSLayoutAttributeLeft
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.contentView
                                                          attribute:NSLayoutAttributeLeft
                                                         multiplier:1
                                                           constant:0]];

    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.imageView
                                                          attribute:NSLayoutAttributeHeight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.contentView
                                                          attribute:NSLayoutAttributeHeight
                                                         multiplier:1
                                                           constant:0]];

    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.imageView
                                                          attribute:NSLayoutAttributeWidth
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.contentView
                                                          attribute:NSLayoutAttributeWidth
                                                         multiplier:1
                                                           constant:0]];
}

- (void)setImageURL:(NSURL *)imageURL
{
    _imageURL = imageURL;
    [self.imageView hnk_setImageFromURL:imageURL];
}

- (void)animateWithCompletion:(void(^)(void))completion
{
    POPSpringAnimation *scaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    scaleAnimation.velocity = [NSValue valueWithCGSize:CGSizeMake(15.f, 15.f)];
    scaleAnimation.toValue = [NSValue valueWithCGSize:CGSizeMake(1.f, 1.f)];
    scaleAnimation.springBounciness = 18.0f;
    [scaleAnimation setCompletionBlock:^(POPAnimation *anim, BOOL finished)
    {
        if (completion)
        {
            completion();
        }
    }];
    [self.layer pop_addAnimation:scaleAnimation forKey:@"layerScaleSpringAnimation"];
}

@end
