//
// Created by VIPER
// Copyright (c) 2015 VIPER. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IVCollectionModuleProtocols.h"
#import "IVCollectionModuleView.h"
#import "IVCollectionModuleAPIDataManager.h"
#import "IVCollectionModuleInteractor.h"
#import "IVCollectionModulePresenter.h"
#import "IVCollectionModuleWireframe.h"
#import <UIKit/UIKit.h>

@interface IVCollectionModuleWireFrame : NSObject <IVCollectionModuleWireFrameProtocol>

@end
