//
// Created by VIPER
// Copyright (c) 2015 VIPER. All rights reserved.
//

#import "IVCollectionModuleWireFrame.h"

@implementation IVCollectionModuleWireFrame

+ (void)presentIVCollectionModuleModuleFromWindow:(UIWindow *)window
{
    // Generating module components
    IVCollectionModuleView* view = [IVCollectionModuleView collectionModuleView];
    id <IVCollectionModulePresenterProtocol, IVCollectionModuleInteractorOutputProtocol> presenter = [IVCollectionModulePresenter new];
    id <IVCollectionModuleInteractorInputProtocol> interactor = [IVCollectionModuleInteractor new];
    id <IVCollectionModuleAPIDataManagerInputProtocol> APIDataManager = [IVCollectionModuleAPIDataManager new];
    id <IVCollectionModuleWireFrameProtocol> wireFrame= [IVCollectionModuleWireFrame new];

    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireFrame = wireFrame;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.APIDataManager = APIDataManager;

    window.rootViewController = [[UINavigationController alloc] initWithRootViewController:view];
    [window makeKeyAndVisible];
}

@end
