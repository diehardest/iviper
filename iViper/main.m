//
//  main.m
//  iViper
//
//  Created by Dmitriy Gorbel on 4/13/15.
//  Copyright (c) 2015 Dmitriy Gorbel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IVAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([IVAppDelegate class]));
    }
}
